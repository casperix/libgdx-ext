package casperix.demo.custom

import casperix.demo.impl.shape_builder.BezierCubicEditor
import casperix.demo.impl.AbstractDemo
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.math.random.nextBezierCubic2f
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random


class BezierCubicDemo(val random: Random) : AbstractDemo() {
    private val bezierBuilder = BezierCubicEditor(pointManager, random.nextBezierCubic2f())

    init {
        pointManager.groups += bezierBuilder.getPoints()
    }

    override fun getDetail(): String {
        val bezier = bezierBuilder.shape
        return listOf(
            "length:" + (bezier.length()).toPrecision(1),
            "p0:" + (bezier.p0).toPrecision(1),
            "p1:" + (bezier.p1).toPrecision(1),
            "p2:" + (bezier.p2).toPrecision(1),
            "p3:" + (bezier.p3).toPrecision(1),
        ).joinToString("\n")
    }

    override fun update(customPoint: Vector2f) {
    }

    override fun renderScene(renderer: Renderer2D) {
        val bezier = bezierBuilder.shape

        renderer.drawCurve( bezier)
        renderer.drawCurvePoints( bezier)
        renderer.drawCurveNormals( bezier)

        renderer.drawPoint(SHAPE_SECONDARY, bezier.p0)
        renderer.drawPoint(SHAPE_SECONDARY, bezier.p1)
        renderer.drawPoint(SHAPE_SECONDARY, bezier.p2)
        renderer.drawPoint(SHAPE_SECONDARY, bezier.p3)
    }


}

