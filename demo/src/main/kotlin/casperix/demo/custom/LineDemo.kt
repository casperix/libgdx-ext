package casperix.demo.custom

import casperix.demo.impl.shape_builder.LineBuilder
import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random


class LineDemo(val random: Random) : AbstractDemo() {
    private val lineBuilder = LineBuilder(random)
    private var curve = lineBuilder.build()
    private val O = ControlPoint(Vector2f(1f))

    private var t = 0f
    private var projection = Vector2f.ZERO

    init {
        pointManager.groups += lineBuilder.getPoints()
        pointManager.groups += listOf(O)
    }

    override fun getDetail(): String {

        return listOf(
            "start: " + (curve.start).toPrecision(1),
            "finish: " + (curve.finish).toPrecision(1),
            "length: " + (curve.length()).toPrecision(1),
            "proj-t: " + t.toPrecision(2),
        ).joinToString("\n")
    }

    override fun update(customPoint: Vector2f) {
        curve = lineBuilder.build()

        t = curve.getProjection(O.position)
        projection = curve.getPosition(t)
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawCurve( curve)
        renderer.drawCurvePoints( curve)
        renderer.drawCurveNormals(curve)

        renderer.drawPoint(SHAPE_SECONDARY, projection)

    }

}

