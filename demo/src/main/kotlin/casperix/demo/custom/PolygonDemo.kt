package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.SelectorAction
import casperix.demo.impl.action.SimpleAction
import casperix.demo.impl.action.TitleEntry
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.shape_builder.PolygonEditor
import casperix.demo.util.RenderConfig.SHAPE_PRIMARY
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.demo.util.RenderConfig.lineThick
import casperix.math.geometry.builder.BorderMode
import casperix.math.geometry.builder.Triangulator
import casperix.math.geometry.float32.getWindingOrder
import casperix.math.geometry.float32.isConvex
import casperix.math.random.nextPolygon2f
import casperix.math.random.nextQuad2f
import casperix.math.random.nextTriangle2f
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random


class PolygonDemo(val random: Random) : AbstractDemo() {
    private val maxOffset = Vector2f(5f)
    private val polygonEditor = PolygonEditor(pointManager, random.nextPolygon2f(maxOffset = maxOffset, vertexAmountRange = 3..6))
    private val O = ControlPoint(Vector2f(0.1f))
    private var border = 0f
    private var borderMode: BorderMode? = BorderMode.CENTER

    init {
        pointManager += listOf(O)
        actionManager.entries += listOf(
            TitleEntry("set border"),
            SelectorAction("border", "center") { borderMode = BorderMode.CENTER },
            SelectorAction("border", "inside") { borderMode = BorderMode.INSIDE },
            SelectorAction("border", "outside") { borderMode = BorderMode.OUTSIDE },
            SelectorAction("border", "off") { borderMode = null },

            TitleEntry("set shape"),
            SimpleAction("triangle") { polygonEditor.setShape(random.nextTriangle2f(maxOffset = maxOffset)) },
            SimpleAction("quad") { polygonEditor.setShape(random.nextQuad2f(maxOffset = maxOffset)) },
        ) + listOf(5, 6, 7, 8).map { vertex ->
            SimpleAction("$vertex-vertex") {
                polygonEditor.setShape(random.nextPolygon2f(maxOffset = maxOffset, vertexAmountRange = vertex..vertex))
            }
        } + SimpleAction("random") {
            polygonEditor.setShape(random.nextPolygon2f(maxOffset = maxOffset))
        }
    }

    override fun getArticles(): List<String> {
        return listOf(
            "convex: ${polygonEditor.shape.isConvex()}",
            "winding: ${polygonEditor.shape.getWindingOrder()}",
            "border: ${border.toPrecision(2)}",
        )
    }

    override fun update(customPoint: Vector2f) {
        border = O.position.length()
    }

    override fun renderScene(renderer: Renderer2D) {
        val border = O.position.length() * 0.1f
        val polygon = polygonEditor.shape
        val borderMode = borderMode

        if (borderMode != null) {
            renderer.drawPolygonWithContour(SHAPE_PRIMARY, SHAPE_SECONDARY, polygon, border, borderMode = borderMode)
        } else {
            renderer.drawPolygon(SHAPE_PRIMARY, polygon)
        }

        if (showHelpers) {
            Triangulator.polygon(polygon).forEach {
                val info = Triangulator.polygonWithContour(it, lineThick, BorderMode.CENTER)
                renderer.drawTriangleList(SHAPE_SECONDARY, info.border)
            }
        }
    }
}

