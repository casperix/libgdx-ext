package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.shape_builder.ArcEditor
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.math.curve.float32.Arc2f
import casperix.math.geometry.Line2f
import casperix.math.random.nextArc2f
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random


class ArcDemo(val random: Random) : AbstractDemo() {
    private val arcEditor = ArcEditor(pointManager, random.nextArc2f())
    private val O = ControlPoint(Vector2f(1f))

    private var t = 0f
    private var projection = Vector2f.ZERO

    init {
        pointManager.groups += arcEditor.getPoints()
        pointManager.groups += listOf(O)
    }

    override fun getArticles(): List<String> {
        val curve = arcEditor.shape
        val additional = if (curve is Arc2f) {
            listOf(
                "arc",
                "center: " + (curve.center).toPrecision(1),
                "start: " + curve.startAngle.toDegree(),
                "delta: " + curve.deltaAngle.toDegree(),
            )
        } else {
            listOf(
                "curve",
                "start: " + curve.start,
                "finish: " + curve.finish,
            )
        }
        return additional + listOf(
            "length: " + (curve.length()).toPrecision(1),
            "proj-t: " + t.toPrecision(2),
        )
    }

    override fun update(customPoint: Vector2f) {
        val curve = arcEditor.shape
        t = curve.getProjection(O.position)
        projection = curve.getPosition(t)
    }

    override fun renderScene(renderer: Renderer2D) {
        val curve = arcEditor.shape
        renderer.drawCurve(curve)
        renderer.drawPoint(SHAPE_SECONDARY, projection)

        if (showNormals) {
            renderer.drawCurvePoints(curve)
            renderer.drawCurveNormals(curve)
        }
        if (showHelpers) {
            renderer.drawLine(SHAPE_SECONDARY, Line2f(curve.start, arcEditor.tangent.position))

            (curve as? Arc2f)?.let { arc ->
                renderer.drawLine(SHAPE_SECONDARY, Line2f(arc.start, arc.center))
                renderer.drawLine(SHAPE_SECONDARY, Line2f(arc.finish, arc.center))
            }
        }
    }

}

