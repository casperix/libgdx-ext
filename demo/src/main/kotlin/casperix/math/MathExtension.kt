package casperix.math

import casperix.math.geometry.CustomPolygon
import casperix.math.geometry.Polygon2f
import casperix.math.geometry.Quad2f
import casperix.math.interpolation.float32.linearInterpolatef
import casperix.math.interpolation.interpolateOf
import casperix.math.test.FloatCompare.uniqueOrdered
import casperix.math.vector.float32.Vector2f

fun Quad2f.split(uParts: Int, vParts: Int): List<Quad2f> {
    return (0 until uParts).flatMap { u ->
        (0 until vParts).map { v ->
            val us = u / uParts.toFloat()
            val uf = (u + 1) / uParts.toFloat()

            val vs = v / vParts.toFloat()
            val vf = (v + 1) / vParts.toFloat()

            //  calculate basis-points on edges of source quad
            val next0 = interpolateOf(v0, v1, us, linearInterpolatef)
            val next1 = interpolateOf(v0, v1, uf, linearInterpolatef)
            val next3 = interpolateOf(v3, v2, us, linearInterpolatef)
            val next2 = interpolateOf(v3, v2, uf, linearInterpolatef)

            //  now calculate final position for sub-quad
            val result0 = interpolateOf(next0, next3, vs, linearInterpolatef)
            val result1 = interpolateOf(next1, next2, vs, linearInterpolatef)
            val result2 = interpolateOf(next1, next2, vf, linearInterpolatef)
            val result3 = interpolateOf(next0, next3, vf, linearInterpolatef)

            Quad2f(result0, result1, result2, result3)
        }
    }
}


fun Polygon2f.splitByEdges(firstIndex: Int, firstPoint: Vector2f, lastIndex: Int, lastPoint: Vector2f): List<Polygon2f> {
    val vertexList = getVertices()

    if (firstIndex > lastIndex) return splitByEdges(lastIndex, lastPoint, firstIndex, firstPoint)

    if (firstIndex == lastIndex) throw Exception("Invalid indices: $firstIndex, $lastIndex")
    if (firstIndex !in vertexList.indices) throw Exception("Invalid first index: $firstIndex")
    if (lastIndex !in vertexList.indices) throw Exception("Invalid last index: $lastIndex")

    val leftPoints = (0 until firstIndex).map {
        vertexList[it]
    } + (lastIndex + 1 until vertexList.size).map {
        vertexList[it]
    }.uniqueOrdered()

    val rightPoints = (0 until firstIndex).map {
        vertexList[it]
    } + (lastIndex + 1 until vertexList.size).map {
        vertexList[it]
    }.uniqueOrdered()

    return listOf(CustomPolygon(leftPoints), CustomPolygon(rightPoints)).filter { it.points.size >= 3 }
}