package casperix.gdx.renderer.impl

import com.badlogic.gdx.graphics.Texture

class GdxTextureConfig(
    var uWrap:Texture.TextureWrap,
    var vWrap:Texture.TextureWrap,
    var minFilter: Texture.TextureFilter,
    var magFilter: Texture.TextureFilter,
    var anisotropicFilter: Float,
)