package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.curve.float32.Arc2f
import casperix.math.curve.float32.Curve2f
import casperix.math.vector.rotateCCW
import casperix.math.vector.rotateCW

class ArcEditor(pointManager: ControlPointManager, initial: Arc2f) : AbstractPointEditor(pointManager), ShapeEditor {
    val start = ControlPoint(initial.start)
    val tangent = ControlPoint()
    val finish = ControlPoint(initial.finish)

    init {
        val toCenter = (initial.center - initial.start).normalize()
        val tangentDirection = if (initial.deltaAngle > 0f) toCenter.rotateCW() else toCenter.rotateCCW()
        tangent.position = start.position + tangentDirection

        selfPoints += listOf(start, tangent, finish)
    }

    override val shape: Curve2f
        get() = Arc2f.byTangent(start.position, tangent.position - start.position, finish.position)
}