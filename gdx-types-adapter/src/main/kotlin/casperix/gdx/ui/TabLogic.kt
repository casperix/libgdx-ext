package casperix.gdx.ui

import casperix.signals.concrete.StorageSignal
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup

/**
 * 	show or hide content around self element
 *
 * 	You can create simple Tab with Button (Tab.build)
 * 	or create yourself custom switcher (and manually call show / hide)
 */
class TabLogic<Switcher : Actor, Content : Actor>(val switcher: Switcher, val content: Content, val contentContainer: WidgetGroup) {
	val onShow = StorageSignal(false)

	init {
		if (switcher is Button) {
			switcher.addSignal(onShow)
		}
		onShow.then { show ->
			if (show) {
				contentContainer.addActor(content)
			} else {
				contentContainer.removeActor(content)
			}
		}
	}
}