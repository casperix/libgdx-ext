package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.math.curve.float32.BezierQuadratic2f
import casperix.math.random.nextVector2f
import casperix.math.vector.float32.Vector2f
import kotlin.random.Random

class BezierQuadraticBuilder(random: Random) : CurveBuilder {
    val p0 = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))
    val p1 = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))
    val p2 = ControlPoint(random.nextVector2f(Vector2f(-1f), Vector2f(1f)))


    override fun getPoints():List<ControlPoint> {
        return listOf(p0, p1, p2)
    }

    override fun build(): BezierQuadratic2f {
        return BezierQuadratic2f(p0.position, p1.position, p2.position)
    }
}