package casperix.demo.custom.search

import casperix.math.axis_aligned.float32.Box2f

data class Element(val area: Box2f)