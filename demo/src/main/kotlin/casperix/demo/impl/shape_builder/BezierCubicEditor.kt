package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.curve.float32.BezierCubic2f

class BezierCubicEditor(pointManager: ControlPointManager, initial: BezierCubic2f) : AbstractPointEditor(pointManager), ShapeEditor {
    val p0 = ControlPoint(initial.p0)
    val p1 = ControlPoint(initial.p1)
    val p2 = ControlPoint(initial.p2)
    val p3 = ControlPoint(initial.p3)

    init {
        selfPoints += listOf(p0, p1, p2, p3)
    }

    override val shape: BezierCubic2f get() = BezierCubic2f(p0.position, p1.position, p2.position, p3.position)

}