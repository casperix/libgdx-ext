package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.vector.float32.Vector2f

class PointEditor(pointManager: ControlPointManager, initial: Vector2f) : AbstractPointEditor(pointManager), ShapeEditor {
    private val center = ControlPoint(initial)

    init {
        selfPoints += listOf(center)
    }

    override val shape: Vector2f get() = center.position
}