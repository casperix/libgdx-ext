package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.GuiEntry
import casperix.demo.impl.action.SelectorAction
import casperix.demo.impl.action.TitleEntry
import casperix.demo.impl.shape_builder.*
import casperix.demo.util.RenderConfig.SHAPE_PRIMARY
import casperix.demo.util.RenderConfig.SHAPE_SECONDARY
import casperix.demo.util.RenderConfig.lineThick
import casperix.math.curve.float32.Curve2f
import casperix.math.geometry.Line2f
import casperix.math.geometry.Polygon2f
import casperix.math.intersection.CurveIntersection
import casperix.math.intersection.float32.Intersection2Float
import casperix.math.random.*
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import kotlin.random.Random


class IntersectionDemo(val random: Random) : AbstractDemo() {
    private var firstBuilder: ShapeEditor = PointEditor(pointManager, random.nextVector2f())
    private var lastBuilder: ShapeEditor = PointEditor(pointManager, random.nextVector2f())

    private var intersectionType = IntersectionType.UNDEFINED
    private var hasIntersection = false
    private var curveIntersectionList = listOf<CurveIntersection>()
    private var mtv:Vector2f? = null

    enum class IntersectionType {
        CURVE_WITH_CURVE,
        POLYGON_WITH_POLYGON,
        UNDEFINED,
    }

    init {
        actionManager.entries += generateBuilders(true)
        actionManager.entries += generateBuilders(false)
    }

    private fun generateBuilders(first: Boolean): List<GuiEntry> {

        return listOf(
            if (first) {
                TitleEntry("set first")
            } else {
                TitleEntry("set last")
            },
            SelectorAction("first:$first", "triangle") { setBuilder(first, TriangleEditor(pointManager, random.nextTriangle2f())) },
            SelectorAction("first:$first", "quad") { setBuilder(first, QuadEditor(pointManager, random.nextQuad2f())) },
            SelectorAction("first:$first", "polygon") { setBuilder(first, PolygonEditor(pointManager, random.nextPolygon2f())) },
            SelectorAction("first:$first", "point") { setBuilder(first, PointEditor(pointManager, random.nextVector2f())) },
            SelectorAction("first:$first", "line") { setBuilder(first, LineEditor(pointManager, random.nextLine2f())) },
            SelectorAction("first:$first", "arc") { setBuilder(first, ArcEditor(pointManager, random.nextArc2f())) },
            SelectorAction("first:$first", "circle") { setBuilder(first, CircleEditor(pointManager, random.nextCircle2f())) },
            SelectorAction("first:$first", "cubic bezier") {
                setBuilder(
                    first,
                    BezierCubicEditor(pointManager, random.nextBezierCubic2f())
                )
            },
            SelectorAction("first:$first", "quadratic bezier") {
                setBuilder(
                    first,
                    BezierQuadraticEditor(pointManager, random.nextBezierQuadratic2f())
                )
            },
        )
    }

    private fun setBuilder(first: Boolean, builder: ShapeEditor) {
        if (first) {
            firstBuilder.dispose()
            firstBuilder = builder
        } else {
            lastBuilder.dispose()
            lastBuilder = builder
        }
        intersectionType = IntersectionType.UNDEFINED
        curveIntersectionList = emptyList()
        hasIntersection = false
    }

    override fun getArticles(): List<String> {
        return listOf(
            "type: $intersectionType",
        )
    }

    override fun update(customPoint: Vector2f) {
        val firstCurve = firstBuilder.shape as? Curve2f
        val lastCurve = lastBuilder.shape as? Curve2f

        val firstPolygon = firstBuilder.shape as? Polygon2f
        val lastPolygon = lastBuilder.shape as? Polygon2f

        if (firstCurve != null && lastCurve != null) {
            intersectionType = IntersectionType.CURVE_WITH_CURVE
            curveIntersectionList = Intersection2Float.getCurveWithCurve(firstCurve, lastCurve)
            hasIntersection = curveIntersectionList.isNotEmpty()
        } else if (firstPolygon != null && lastPolygon != null) {
            intersectionType = IntersectionType.POLYGON_WITH_POLYGON
            hasIntersection = Intersection2Float.hasPolygonWithPolygon(firstPolygon, lastPolygon)
            mtv = Intersection2Float.getPolygonWithPolygon(firstPolygon, lastPolygon)
        } else {
            intersectionType = IntersectionType.UNDEFINED
        }
    }

    override fun renderScene(renderer: Renderer2D) {
        val color = if (hasIntersection) {
            SHAPE_PRIMARY
        } else {
            SHAPE_SECONDARY
        }

        listOfNotNull(firstBuilder.shape as? Curve2f, lastBuilder.shape as? Curve2f).forEach {
            renderer.drawCurve(color, it, lineThick, 1000)
        }

        listOfNotNull(firstBuilder.shape as? Polygon2f, lastBuilder.shape as? Polygon2f).forEach {
            renderer.drawPolygon(color, it)
        }

        curveIntersectionList.forEach {
            renderer.drawPoint(SHAPE_PRIMARY, it.position)
        }

        mtv?.also {
            renderer.drawLine(SHAPE_SECONDARY, Line2f(Vector2f.ZERO, it))
        }
    }
}

