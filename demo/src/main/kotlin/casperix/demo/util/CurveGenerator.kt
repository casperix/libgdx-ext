package casperix.demo.util

import casperix.math.curve.float32.Arc2f
import casperix.math.curve.float32.BezierCubic2f
import casperix.math.curve.float32.BezierQuadratic2f
import casperix.math.curve.float32.LineCurve2f
import casperix.math.geometry.Line2f
import casperix.math.random.nextFloat
import casperix.math.random.nextRadianFloat
import casperix.math.random.nextVector2f
import casperix.math.vector.float32.Vector2f
import kotlin.random.Random

object CurveGenerator {
    val random = Random(1)


    fun nextBezierCubic(): BezierCubic2f {
        val A = random.nextVector2f(Vector2f(-1f, -1f), Vector2f(0f, 0f))
        val B = random.nextVector2f(Vector2f(-1f, 0f), Vector2f(0f, 1f))
        val C = random.nextVector2f(Vector2f(0f, 0f), Vector2f(1f, 1f))
        val D = random.nextVector2f(Vector2f(0f, -1f), Vector2f(1f, 0f))
        return BezierCubic2f(A, B, C, D)
    }

    fun nextBezierQuadratic(): BezierQuadratic2f {
        val A = random.nextVector2f(Vector2f(-1f, -1f), Vector2f(0f, 0f))
        val B = random.nextVector2f(Vector2f(-1f, 0f), Vector2f(0f, 1f))
        val C = random.nextVector2f(Vector2f(0f, 0f), Vector2f(1f, 1f))
        return BezierQuadratic2f(A, B, C)
    }

    fun nextArc(): Arc2f {
        val center = random.nextVector2f(Vector2f(-1f, -1f), Vector2f(1f, 1f))
        val s = random.nextRadianFloat()
        val f = random.nextRadianFloat()
        val r = random.nextFloat(0.1f, 2f)
        return Arc2f(center, s, f, r)
    }

    fun nextLine(): LineCurve2f {
        val A = random.nextVector2f(Vector2f(-1f), Vector2f(1f))
        val B = random.nextVector2f(Vector2f(-1f), Vector2f(1f))
        return LineCurve2f(Line2f(A, B))
    }
}