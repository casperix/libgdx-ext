package casperix.gdx.ui

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.ui.Button

class ToggleGroup(val buttons: List<Button>, selected: Button?) {

    constructor(buttons: List<Button>, selectFirst: Boolean) : this(buttons, if (selectFirst) buttons.firstOrNull() else null)

    init {
        buttons.forEach {
            it.addClickListener {
                click(it)
            }
        }

        if (selected?.isChecked == false) {
            selected.simulateClick()
        }
    }

    private fun click(current: Button) {
        if (!current.isChecked) return

        buttons.forEach {
            it.isChecked = it == current
        }
    }

    companion object {
        fun from(actor: Actor, selectFirst: Boolean): ToggleGroup {
            val buttons = extractButtons(actor)
            return ToggleGroup(buttons, selectFirst)
        }

        private fun extractButtons(actor: Actor): List<Button> {
            if (actor is Button) {
                return listOf(actor)
            }

            if (actor is Group) {
                return actor.children.flatMap {
                    extractButtons(it)
                }
            }

            return emptyList()
        }
    }
}