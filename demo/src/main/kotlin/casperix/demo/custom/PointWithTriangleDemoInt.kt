package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.math.color.Colors
import casperix.math.geometry.Line2i
import casperix.math.intersection.int32.Intersection2Int
import casperix.math.random.nextTriangle2i
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.int32.Vector2i
import casperix.renderer.Renderer2D
import kotlin.random.Random

class PointWithTriangleDemoInt(val random: Random) : AbstractDemo() {

    val triangle = random.nextTriangle2i(-10, 10)
    var point = Vector2i.ZERO
    var hasIntersection = false
    var hasBorder = false


    override fun update(customPoint: Vector2f) {
        point = customPoint.roundToVector2i()

        hasIntersection = Intersection2Int.hasPointWithTriangle(point, triangle)

        hasBorder = Intersection2Int.hasPointWithLine(point, Line2i(triangle.v0, triangle.v1)) ||
                Intersection2Int.hasPointWithLine(point, Line2i(triangle.v1, triangle.v2)) ||
                Intersection2Int.hasPointWithLine(point, Line2i(triangle.v2, triangle.v0))
    }

    override fun getDetail(): String {

        return "Intersection: $hasIntersection\nBorder: $hasBorder"
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawTriangle(Colors.GREEN, triangle.convert { it.toVector2f() })

        val pointColor = if (hasBorder) {
            Colors.RED
        } else if (hasIntersection) {
            Colors.WHITE
        } else {
            Colors.BLUE
        }
        renderer.drawPoint(pointColor, point)
    }
}