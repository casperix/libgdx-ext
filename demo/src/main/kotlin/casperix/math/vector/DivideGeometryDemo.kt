package casperix.math.vector

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.SwitchAction
import casperix.demo.impl.shape_builder.LineEditor
import casperix.demo.impl.shape_builder.PolygonEditor
import casperix.math.mesh.float32.Mesh
import casperix.math.random.nextLine2f
import casperix.math.random.nextPolygon2f
import casperix.math.vector.float32.Vector2f
import casperix.mesh.city.component.ShapeBuilder
import casperix.mesh.city.render.CityMeshRender
import casperix.renderer.Renderer2D
import kotlin.random.Random


class DivideGeometryDemo(val random: Random) : AbstractDemo() {
    private val quadEditor = PolygonEditor(pointManager, random.nextPolygon2f(maxOffset = Vector2f(5f), vertexAmountRange = 3..6))
    private val lineEditor1 = LineEditor(pointManager, random.nextLine2f())
    private val lineEditor2 = LineEditor(pointManager, random.nextLine2f())

    private var input = Mesh()
    private var output = Mesh()
    private var infinity = false

    init {
        actionManager += SwitchAction("infinity") {
            infinity = it
        }
    }

    override fun getArticles(): List<String> {
        return listOf(
            "input: " + input.regions.size,
            "output: " + output.regions.size,
        )
    }

    override fun update(customPoint: Vector2f) {

        input = ShapeBuilder.create(quadEditor.shape)


        val stage1 = ShapeBuilder.divide(input, lineEditor1.segment)
        val stage2 = ShapeBuilder.divide(stage1, lineEditor2.segment)


        output = stage2
    }

    override fun renderScene(renderer: Renderer2D) {
        CityMeshRender.render(renderer, output, false, true, true)

    }

}

