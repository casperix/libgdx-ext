package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.control_point.ControlPoint
import casperix.math.angle.float32.DegreeFloat
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D
import kotlin.random.Random


class AngularDemo(val random: Random) : AbstractDemo() {
    val O = ControlPoint(Vector2f(0f))
    val A = ControlPoint(Vector2f(1f, 0f))
    val B = ControlPoint(Vector2f(0f, 1f))

    init {
        pointManager.groups += listOf(O, A, B)
    }

    override fun getDetail(): String {
        val angle1 = DegreeFloat.byDirection(A.position - O.position)
        val angle2 = DegreeFloat.byDirection(B.position - O.position)
        val dest = DegreeFloat.betweenDirections(A.position - O.position, B.position - O.position)

        return listOf(
            "angle1: ${angle1.format()}",
            "angle2: ${angle2.format()}",
            "dest: ${dest.format()}",
        ).joinToString("\n")
    }

    override fun update(customPoint: Vector2f) {
    }

    override fun renderScene(renderer: Renderer2D) {

    }
}

