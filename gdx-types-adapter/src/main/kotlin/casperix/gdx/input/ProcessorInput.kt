package casperix.gdx.input

import casperix.input.InputDispatcher
import casperix.misc.Disposable
import casperix.signals.then
import com.badlogic.gdx.InputProcessor
import kotlin.math.roundToInt

class ProcessorInput(holder: MutableCollection<in Disposable>, source: InputDispatcher, val receiver: InputProcessor) {
	init {
		source.onKeyDown.then(holder) {
			receiver.keyDown(it.button.code)
		}
		source.onKeyUp.then(holder) {
			receiver.keyUp(it.button.code)
		}
		source.onKeyTyped.then(holder) {
			receiver.keyTyped(it.char)
		}
		source.onMouseMove.then(holder) {
			receiver.mouseMoved(it.position.x.roundToInt(), it.position.y.roundToInt())
		}
		source.onMouseWheel.then(holder) {
			receiver.scrolled(it.wheel.x.toFloat(), it.wheel.y.toFloat())
		}
		source.onTouchDown.then(holder) {
			receiver.touchDown(it.position.x.roundToInt(), it.position.y.roundToInt(), it.pointer, it.button.code)
		}
		source.onTouchUp.then(holder) {
			receiver.touchUp(it.position.x.roundToInt(), it.position.y.roundToInt(), it.pointer, it.button.code)
		}
		source.onTouchDragged.then(holder) {
			receiver.touchDragged(it.position.x.roundToInt(), it.position.y.roundToInt(), it.pointer)
		}
	}
}