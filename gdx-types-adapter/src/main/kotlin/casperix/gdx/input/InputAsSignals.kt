package casperix.gdx.input

import casperix.input.*
import casperix.math.vector.float32.Vector2f
import casperix.math.vector.int32.Vector2i
import casperix.signals.concrete.Signal
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputAdapter

class InputAsSignals : InputAdapter(), InputDispatcher {
    private var lastButton: KeyButton? = null

//    override val onButtonDown = Signal<ButtonEvent>()
//
//    override val onButtonUp = Signal<ButtonEvent>()

    override val onKeyDown = Signal<KeyDown>()

    override val onKeyTyped = Signal<KeyTyped>()

    override val onKeyUp = Signal<KeyUp>()

    override val onMouseMove = Signal<MouseMove>()

    override val onMouseWheel = Signal<MouseWheel>()

    override val onTouchDown = Signal<TouchDown>()

    override val onTouchDragged = Signal<TouchMove>()

    override val onTouchUp = Signal<TouchUp>()


    fun isCtrl(): Boolean {
        return Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)
    }

    fun isAlt(): Boolean {
        return Gdx.input.isKeyPressed(Input.Keys.ALT_LEFT) || Gdx.input.isKeyPressed(Input.Keys.ALT_RIGHT)
    }

    override fun keyDown(keycode: Int): Boolean {
        val event = KeyDown(KeyButton.getByCode(keycode))
        lastButton = event.button
        onKeyDown.set(event)
        return event.captured
    }

    override fun keyUp(keycode: Int): Boolean {
        val event = KeyUp(KeyButton.getByCode(keycode))
        onKeyUp.set(event)
        return event.captured
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        val event = TouchUp(
            getMousePosition(screenX, screenY),
            pointer,
            PointerButton(button),
//            getMouseNormalized(screenX, screenY),
//            getMouseMovement()
        )
        onTouchUp.set(event)
        return event.captured
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        val event =
            MouseMove(getMousePosition(screenX, screenY))//, getMouseNormalized(screenX, screenY), getMouseMovement())
        onMouseMove.set(event)
        return event.captured
    }

    override fun keyTyped(character: Char): Boolean {
        val event = KeyTyped(character, lastButton!!)
        onKeyTyped.set(event)
        return event.captured
    }

    override fun scrolled(x: Float, y: Float): Boolean {
        val event = MouseWheel(
            getMousePosition(Gdx.input.x, Gdx.input.y),
//            getMouseNormalized(0, 0),
            Vector2f(x, y)
        )
        onMouseWheel.set(event)
        return event.captured
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        val event = TouchMove(
            getMousePosition(screenX, screenY),
            pointer,
//            getMouseNormalized(screenX, screenY),
//            getMouseMovement()
        )
        onTouchDragged.set(event)
        return event.captured
    }


    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        val event = TouchDown(
            getMousePosition(screenX, screenY),
            pointer,
            PointerButton(button),
//            getMouseNormalized(screenX, screenY),
//            getMouseMovement()
        )
        onTouchDown.set(event)
        return event.captured
    }


    private fun getMouseMovement(): Vector2f {
        return Vector2i(Gdx.input.deltaX, Gdx.input.deltaY).toVector2f()
    }

    private fun getMousePosition(sourceX: Int, sourceY: Int): Vector2f {
        return Vector2i(sourceX, sourceY).toVector2f()
    }

    private fun getMouseNormalized(sourceX: Int, sourceY: Int): Vector2f {
        val screen = Vector2i(Gdx.graphics.width, Gdx.graphics.height).toVector2f()
        return Vector2i(sourceX, sourceY).toVector2f() / screen
    }

}