package casperix.demo.custom.search

import casperix.demo.util.naive_search.SpatialEntry2D
import casperix.demo.util.naive_search.SpatialSearchBySortedOrdinates
import casperix.math.axis_aligned.float32.Box2f
import casperix.math.curve.float32.Circle2f

class SpaceMapAdapter : Adapter {
    private val container = SpatialSearchBySortedOrdinates<Element>()
    override fun clear() {
        container.clear()
    }

    override fun add(element: Element) {
        container.add(SpatialEntry2D(element.area.min, element))
    }

    override fun search(area: Box2f): Collection<Element> {
        return container.searchValues(area)
    }

    override fun search(circle: Circle2f): Collection<Element> {
        return container.searchValues(Box2f.ONE)
    }

}