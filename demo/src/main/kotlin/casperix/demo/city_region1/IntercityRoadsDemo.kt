@file:OptIn(ExperimentalUnsignedTypes::class)

package casperix.demo.city_region1

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.action.SelectorAction
import casperix.demo.impl.action.SimpleAction
import casperix.demo.impl.action.TitleEntry
import casperix.demo.impl.control_point.ControlPoint
import casperix.math.random.nextVector2f
import casperix.math.straight_line.float32.LineSegment2f
import casperix.math.vector.float32.Vector2f
import casperix.mesh.city.component.IntercityRoadsGenerator
import casperix.mesh.city.component.LengthMode
import casperix.mesh.city.component.PointBasedPathConfig
import casperix.misc.clamp
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random

class IntercityRoadsDemo(val random: Random) : AbstractDemo() {
    private val INITIAL_AMOUNT = 5
    private val cities = mutableListOf<ControlPoint>()

    private var roads: List<LineSegment2f> = emptyList()

    private var config = PointBasedPathConfig()

    init {
        pointManager.groups += cities
        pointManager.drawBack = false

        actionManager.entries += listOf(
            TitleEntry("cities"),
            SimpleAction("add", ::addCity),
            SimpleAction("remove", ::removeCity),

            TitleEntry("length-mode"),
            SelectorAction("length", "default") { setConfig(config.copy(lengthMode = LengthMode.DEFAULT)) },
            SelectorAction("length", "squared") { setConfig(config.copy(lengthMode = LengthMode.SQUARED)) },
            SelectorAction("length", "inf") { setConfig(config.copy(lengthMode = LengthMode.INF)) },
            SelectorAction("length", "one") { setConfig(config.copy(lengthMode = LengthMode.ONE)) },

            TitleEntry("way-factor"),
            SimpleAction("increase") {
                setConfig(
                    config.copy(
                        wayFactor = (config.wayFactor * 1.05f).clamp(
                            1f,
                            100f
                        )
                    )
                )
            },
            SimpleAction("decrease") {
                setConfig(
                    config.copy(
                        wayFactor = (config.wayFactor * 0.95f).clamp(
                            1f,
                            100f
                        )
                    )
                )
            },
        )

        repeat(INITIAL_AMOUNT) {
            addCity()
        }

        pointManager.pointDragged.then {
            updateRoads()
        }

        updateRoads()
    }

    private fun setConfig(config: PointBasedPathConfig) {
        this.config = config
        updateRoads()
    }

    private fun updateRoads() {
        roads = IntercityRoadsGenerator.generateRoads(config, cities.map {
            it.position
        })
    }

    private fun removeCity() {
        val next = cities.lastOrNull() ?: return
        cities -= next
        updateRoads()
    }

    private fun addCity() {
        val next = ControlPoint(random.nextVector2f(-5f, 5f))
        cities += next
        updateRoads()
    }

    override fun getArticles(): List<String> {
        return listOf(
            "length: ${config.lengthMode}",
            "way factor: ${config.wayFactor.toPrecision(2)}",
        )
    }

    override fun update(customPoint: Vector2f) {

    }

    override fun renderScene(renderer: Renderer2D) {

        roads.forEach { road ->
            val city = cities.first { it.position == road.start || it.position == road.finish }
            val index = cities.indexOf(city)
            val color = pointManager.BACK_COLORS[index % pointManager.BACK_COLORS.size]

            renderer.drawSegment(color, road)
        }
    }

}

