package casperix.app

interface SafeApplicationAdapter {
    fun resize(width: Int, height: Int)
    fun render()
    fun dispose()
}