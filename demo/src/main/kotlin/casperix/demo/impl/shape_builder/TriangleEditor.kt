package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.geometry.Triangle2f

class TriangleEditor(pointManager: ControlPointManager, initial: Triangle2f) : AbstractPointEditor(pointManager), ShapeEditor {
    val A = ControlPoint(initial.v0)
    val B = ControlPoint(initial.v1)
    val C = ControlPoint(initial.v2)

    val triangle: Triangle2f get() = Triangle2f(A.position, B.position, C.position)

    init {
        selfPoints += listOf(A, B, C)
    }

    override val shape: Any get() = triangle

}