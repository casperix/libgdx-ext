package casperix.gdx.renderer.impl

import casperix.renderer.pixel_map.PixelMap
import casperix.renderer.pixel_map.codec.RGBACodec
import casperix.renderer.pixel_map.codec.RGBCodec
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.Texture.TextureFilter

@ExperimentalUnsignedTypes
class GdxTextureProvider {
    companion object {
        val config = GdxTextureConfig(
            Texture.TextureWrap.Repeat,
            Texture.TextureWrap.Repeat,
            TextureFilter.MipMapNearestLinear,
            TextureFilter.Linear,
            1f,
        )
    }

    private val map = mutableMapOf<PixelMap, Texture>()
    fun get(bitmap: PixelMap): Texture {
        return map.getOrPut(bitmap) {
            createTexture(bitmap)
        }
    }

    private fun pixmapToTexture(pixmap: Pixmap): Texture {
        return Texture(pixmap, true).apply {
            pixmap.dispose()
            setWrap(config.uWrap, config.vWrap)
            setFilter(config.minFilter, config.magFilter)
            setAnisotropicFilter(config.anisotropicFilter)
        }
    }

    private fun createTexture(bitmap: PixelMap): Texture {
        val gdxFormat = when (bitmap.pixelCodec) {
            RGBACodec  -> Pixmap.Format.RGBA8888
            RGBCodec -> Pixmap.Format.RGB888
            else -> throw Exception("Unsupported format: ${bitmap.pixelCodec}")
        }

        val pixmap = Pixmap(bitmap.width, bitmap.height, gdxFormat)
        pixmap.pixels.put(bitmap.bytes.data.toByteArray())
        pixmap.pixels.flip()
        return pixmapToTexture(pixmap)
    }
}