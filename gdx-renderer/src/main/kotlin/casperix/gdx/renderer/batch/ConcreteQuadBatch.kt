package casperix.gdx.renderer.batch

import casperix.gdx.renderer.impl.MaterialShader
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import kotlin.math.min


class ConcreteQuadBatch(maxVertices: Int, maxTriangles: Int, defaultShader: ShaderProgram?) : MaterialBatch {
    val VERTEX_SIZE = 2 + 1 + 2 + 2
    val QUAD_SPRITE_SIZE = 4 * VERTEX_SIZE
    val TRIANGLE_SPRITE_SIZE = 3 * VERTEX_SIZE

    private val mesh: Mesh
    private val vertices: FloatArray
    private val triangles: ShortArray
    private var vertexIndex = 0
    private var triangleIndex = 0
    private var lastAlbedo: Texture? = null
    private var lastNormal: Texture? = null
    private var drawing = false
    private val transformMatrix = Matrix4()
    private val projectionMatrix = Matrix4()
    private val combinedMatrix = Matrix4()
    private var blendingDisabled = false
    private var blendSrcFunc = GL20.GL_SRC_ALPHA
    private var blendDstFunc = GL20.GL_ONE_MINUS_SRC_ALPHA
    private var blendSrcFuncAlpha = GL20.GL_SRC_ALPHA
    private var blendDstFuncAlpha = GL20.GL_ONE_MINUS_SRC_ALPHA
    private var shader: ShaderProgram? = null
    private var customShader: ShaderProgram? = null
    private var ownsShader = false
    private val color = Color(1f, 1f, 1f, 1f)
    var colorPacked = Color.WHITE_FLOAT_BITS

    override var lightPosition = Vector3(1000f, 1000f, 1000f)
    override var ambientColor = Color(0.2f, 0.2f, 0.2f, 1f)

    /** Number of render calls since the last [.begin].  */
    var renderCalls = 0

    /** Number of rendering calls, ever. Will not be reset unless set manually.  */
    var totalRenderCalls = 0

    /** The maximum number of triangles rendered in one batch so far.  */
    var maxTrianglesInBatch = 0

    /** Constructs a PolygonSpriteBatch with the default shader, size vertices, and size * 2 triangles.
     * @param size The max number of vertices and number of triangles in a single batch. Max of 32767.
     * @see .PolygonSpriteBatch
     */
    constructor(size: Int) : this(size, size * 2, null)
    /** Constructs a PolygonSpriteBatch with the specified shader, size vertices and size * 2 triangles.
     * @param size The max number of vertices and number of triangles in a single batch. Max of 32767.
     * @see .PolygonSpriteBatch
     */
    /** Constructs a PolygonSpriteBatch with the default shader, 2000 vertices, and 4000 triangles.
     * @see .PolygonSpriteBatch
     */

    /** Constructs a new PolygonSpriteBatch. Sets the projection matrix to an orthographic projection with y-axis point upwards,
     * x-axis point to the right and the origin being in the bottom left corner of the screen. The projection will be pixel perfect
     * with respect to the current screen resolution.
     *
     *
     * The defaultShader specifies the shader to use. Note that the names for uniforms for this default shader are different than
     * the ones expect for shaders set with [.setShader]. See [SpriteBatch.createDefaultShader].
     * @param maxVertices The max number of vertices in a single batch. Max of 32767.
     * @param maxTriangles The max number of triangles in a single batch.
     * @param defaultShader The default shader to use. This is not owned by the PolygonSpriteBatch and must be disposed separately.
     * May be null to use the default shader.
     */
    init {
        // 32767 is max vertex index.
        require(maxVertices <= 32767) { "Can't have more than 32767 vertices per batch: $maxVertices" }
        var vertexDataType = Mesh.VertexDataType.VertexArray
        if (Gdx.gl30 != null) {
            vertexDataType = Mesh.VertexDataType.VertexBufferObjectWithVAO
        }
        mesh = Mesh(
            vertexDataType, false, maxVertices, maxTriangles * 3,
            VertexAttribute(VertexAttributes.Usage.Position, 2, ShaderProgram.POSITION_ATTRIBUTE),
            VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE),
            VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE + "0"),
            VertexAttribute(VertexAttributes.Usage.Tangent, 2, ShaderProgram.TANGENT_ATTRIBUTE),
        )
        vertices = FloatArray(maxVertices * VERTEX_SIZE)
        triangles = ShortArray(maxTriangles * 3)
        if (defaultShader == null) {
            shader = MaterialShader().program
            ownsShader = true
        } else shader = defaultShader
        projectionMatrix.setToOrtho2D(0f, 0f, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
    }

    override fun begin() {
        check(!drawing) { "PolygonSpriteBatch.end must be called before begin." }
        renderCalls = 0
        Gdx.gl.glDepthMask(false)
        if (customShader != null) customShader!!.bind() else shader!!.bind()
        setupMatrices()
        drawing = true
    }

    override fun end() {
        check(drawing) { "PolygonSpriteBatch.begin must be called before end." }
        if (vertexIndex > 0) flush()
        lastAlbedo = null
        lastNormal = null

        drawing = false
        val gl = Gdx.gl
        gl.glDepthMask(true)
        Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0)

        if (isBlendingEnabled()) gl.glDisable(GL20.GL_BLEND)
    }

    fun setColor(tint: Color) {
        color.set(tint)
        colorPacked = tint.toFloatBits()
    }

    fun setColor(r: Float, g: Float, b: Float, a: Float) {
        color[r, g, b] = a
        colorPacked = color.toFloatBits()
    }

    fun setPackedColor(packedColor: Float) {
        Color.abgr8888ToColor(color, packedColor)
        colorPacked = packedColor
    }

    fun getColor(): Color {
        return color
    }

    fun getPackedColor(): Float {
        return colorPacked
    }

    override fun drawQuad(albedo: Texture, normal: Texture, spriteVertices: FloatArray, offset: Int, count: Int) {
        var offset = offset
        var count = count
        check(drawing) { "PolygonSpriteBatch.begin must be called before draw." }
        val triangles = triangles
        val vertices = vertices
        var triangleCount = count / QUAD_SPRITE_SIZE * 6
        var batch: Int
        if (albedo !== lastAlbedo || normal !== lastNormal) {
            switchTexture(albedo, normal)
            batch = min(
                min(count.toDouble(), (vertices.size - vertices.size % QUAD_SPRITE_SIZE).toDouble()),
                (triangles.size / 6 * QUAD_SPRITE_SIZE).toDouble()
            )
                .toInt()
            triangleCount = batch / QUAD_SPRITE_SIZE * 6
        } else if (triangleIndex + triangleCount > triangles.size || vertexIndex + count > vertices.size) {
            flush()
            batch = min(
                min(count.toDouble(), (vertices.size - vertices.size % QUAD_SPRITE_SIZE).toDouble()),
                (triangles.size / 6 * QUAD_SPRITE_SIZE).toDouble()
            )
                .toInt()
            triangleCount = batch / QUAD_SPRITE_SIZE * 6
        } else batch = count
        var vertexIndex = vertexIndex
        var vertex = (vertexIndex / VERTEX_SIZE).toShort()
        var triangleIndex = triangleIndex
        val n = triangleIndex + triangleCount
        while (triangleIndex < n) {
            triangles[triangleIndex] = vertex
            triangles[triangleIndex + 1] = (vertex + 1).toShort()
            triangles[triangleIndex + 2] = (vertex + 2).toShort()
            triangles[triangleIndex + 3] = (vertex + 2).toShort()
            triangles[triangleIndex + 4] = (vertex + 3).toShort()
            triangles[triangleIndex + 5] = vertex
            triangleIndex += 6
            vertex = (vertex + 4).toShort()
        }
        while (true) {
            System.arraycopy(spriteVertices, offset, vertices, vertexIndex, batch)
            this.vertexIndex = vertexIndex + batch
            this.triangleIndex = triangleIndex
            count -= batch
            if (count == 0) break
            offset += batch
            flush()
            vertexIndex = 0
            if (batch > count) {
                batch = min(count.toDouble(), (triangles.size / 6 * QUAD_SPRITE_SIZE).toDouble()).toInt()
                triangleIndex = batch / QUAD_SPRITE_SIZE * 6
            }
        }
    }


    override fun drawTriangle(albedo: Texture, normal: Texture, spriteVertices: FloatArray, offset: Int, count: Int) {
        var offset = offset
        var count = count
        check(drawing) { "PolygonSpriteBatch.begin must be called before draw." }
        val triangles = triangles
        val vertices = vertices
        var triangleCount = count / TRIANGLE_SPRITE_SIZE * 3
        var batch: Int
        if (albedo !== lastAlbedo || normal !== lastNormal) {
            switchTexture(albedo, normal)
            batch = min(
                min(count.toDouble(), (vertices.size - vertices.size % TRIANGLE_SPRITE_SIZE).toDouble()),
                (triangles.size / 3 * TRIANGLE_SPRITE_SIZE).toDouble()
            )
                .toInt()
            triangleCount = batch / TRIANGLE_SPRITE_SIZE * 3
        } else if (triangleIndex + triangleCount > triangles.size || vertexIndex + count > vertices.size) {
            flush()
            batch = min(
                min(count.toDouble(), (vertices.size - vertices.size % TRIANGLE_SPRITE_SIZE).toDouble()),
                (triangles.size / 3 * TRIANGLE_SPRITE_SIZE).toDouble()
            )
                .toInt()
            triangleCount = batch / TRIANGLE_SPRITE_SIZE * 3
        } else batch = count
        var vertexIndex = vertexIndex
        var vertex = (vertexIndex / VERTEX_SIZE).toShort()
        var triangleIndex = triangleIndex
        val n = triangleIndex + triangleCount
        while (triangleIndex < n) {
            triangles[triangleIndex] = vertex
            triangles[triangleIndex + 1] = (vertex + 1).toShort()
            triangles[triangleIndex + 2] = (vertex + 2).toShort()
            triangleIndex += 3
            vertex = (vertex + 3).toShort()
        }
        while (true) {
            System.arraycopy(spriteVertices, offset, vertices, vertexIndex, batch)
            this.vertexIndex = vertexIndex + batch
            this.triangleIndex = triangleIndex
            count -= batch
            if (count == 0) break
            offset += batch
            flush()
            vertexIndex = 0
            if (batch > count) {
                batch = min(count.toDouble(), (triangles.size / 3 * TRIANGLE_SPRITE_SIZE).toDouble()).toInt()
                triangleIndex = batch / TRIANGLE_SPRITE_SIZE * 3
            }
        }
    }


    fun flush() {
        if (vertexIndex == 0) return
        renderCalls++
        totalRenderCalls++
        val trianglesInBatch = triangleIndex
        if (trianglesInBatch > maxTrianglesInBatch) maxTrianglesInBatch = trianglesInBatch
        lastAlbedo!!.bind(0)
        lastNormal?.bind(1)

        val mesh = mesh
        mesh.setVertices(vertices, 0, vertexIndex)
        mesh.setIndices(triangles, 0, trianglesInBatch)
        if (blendingDisabled) {
            Gdx.gl.glDisable(GL20.GL_BLEND)
        } else {
            Gdx.gl.glEnable(GL20.GL_BLEND)
            if (blendSrcFunc != -1) Gdx.gl.glBlendFuncSeparate(
                blendSrcFunc,
                blendDstFunc,
                blendSrcFuncAlpha,
                blendDstFuncAlpha
            )
        }
        mesh.render(if (customShader != null) customShader else shader, GL20.GL_TRIANGLES, 0, trianglesInBatch)
        vertexIndex = 0
        triangleIndex = 0
    }

    fun disableBlending() {
        flush()
        blendingDisabled = true
    }

    fun enableBlending() {
        flush()
        blendingDisabled = false
    }

    fun setBlendFunction(srcFunc: Int, dstFunc: Int) {
        setBlendFunctionSeparate(srcFunc, dstFunc, srcFunc, dstFunc)
    }

    fun setBlendFunctionSeparate(srcFuncColor: Int, dstFuncColor: Int, srcFuncAlpha: Int, dstFuncAlpha: Int) {
        if (blendSrcFunc == srcFuncColor && blendDstFunc == dstFuncColor && blendSrcFuncAlpha == srcFuncAlpha && blendDstFuncAlpha == dstFuncAlpha) return
        flush()
        blendSrcFunc = srcFuncColor
        blendDstFunc = dstFuncColor
        blendSrcFuncAlpha = srcFuncAlpha
        blendDstFuncAlpha = dstFuncAlpha
    }

    fun getBlendSrcFunc(): Int {
        return blendSrcFunc
    }

    fun getBlendDstFunc(): Int {
        return blendDstFunc
    }

    fun getBlendSrcFuncAlpha(): Int {
        return blendSrcFuncAlpha
    }

    fun getBlendDstFuncAlpha(): Int {
        return blendDstFuncAlpha
    }

    fun dispose() {
        mesh.dispose()
        if (ownsShader && shader != null) shader!!.dispose()
    }

    fun getProjectionMatrix(): Matrix4 {
        return projectionMatrix
    }

    fun getTransformMatrix(): Matrix4 {
        return transformMatrix
    }

    fun setProjectionMatrix(projection: Matrix4) {
        if (drawing) flush()
        projectionMatrix.set(projection)
        if (drawing) setupMatrices()
    }

    fun setTransformMatrix(transform: Matrix4) {
        if (drawing) flush()
        transformMatrix.set(transform)
        if (drawing) setupMatrices()
    }

    protected fun setupMatrices() {
        combinedMatrix.set(projectionMatrix).mul(transformMatrix)
        val actualShader = customShader ?: shader ?: return
        actualShader.setUniformi("u_texture_albedo", 0)
        actualShader.setUniformi("u_texture_normal", 1)
        actualShader.setUniformf("u_light_pos", lightPosition.x, lightPosition.y, lightPosition.z)
        actualShader.setUniformMatrix("u_projection_view", combinedMatrix)
        actualShader.setUniformi("u_texture_albedo", 0)
        actualShader.setUniformi("u_texture_normal", 1)
        actualShader.setUniformf("u_ambient_color", ambientColor.r, ambientColor.g, ambientColor.b)
    }

    private fun switchTexture(albedo: Texture, normal: Texture) {
        flush()
        lastAlbedo = albedo
        lastNormal = normal
    }

    fun setShader(shader: ShaderProgram) {
        if (drawing) {
            flush()
        }
        customShader = shader
        if (drawing) {
            if (customShader != null) customShader!!.bind() else this.shader!!.bind()
            setupMatrices()
        }
    }

    fun getShader(): ShaderProgram {
        return if (customShader == null) {
            shader!!
        } else customShader!!
    }

    fun isBlendingEnabled(): Boolean {
        return !blendingDisabled
    }

    fun isDrawing(): Boolean {
        return drawing
    }
}