package casperix.gdx.renderer.impl

import casperix.gdx.geometry.toMatrix4
import casperix.gdx.geometry.toVector3
import casperix.gdx.graphics.toColor
import casperix.gdx.graphics.triangle
import casperix.gdx.renderer.batch.ConcreteQuadBatch
import casperix.math.axis_aligned.int32.Box2i
import casperix.math.axis_aligned.int32.Dimension2i
import casperix.math.color.Colors
import casperix.math.color.float32.Color4f
import casperix.math.geometry.PointAroundRay
import casperix.math.geometry.Quad2f
import casperix.math.geometry.Triangle2f
import casperix.math.geometry.float32.Geometry2Float
import casperix.math.quad_matrix.float32.Matrix3f
import casperix.math.quad_matrix.float32.Matrix4f
import casperix.math.vector.float32.Vector3f
import casperix.math.vector.vectorOf
import casperix.misc.Disposable
import casperix.misc.min
import casperix.renderer.Environment
import casperix.renderer.Renderer2D
import casperix.renderer.material.Material
import casperix.renderer.vector.Geometry
import casperix.renderer.vector.VectorGraphic
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL20.GL_SCISSOR_TEST
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.graphics.glutils.HdpiUtils.glViewport
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4

@ExperimentalUnsignedTypes
class GdxRenderer2D : Renderer2D, Disposable {

    enum class RenderState {
        NOTHING,
        COLORED,
        TEXTURED,
        MATERIAL,
    }

    private val MAX_TRIANGLES_IN_BATCH = 1000

    private val textureProvider = GdxTextureProvider()
    private val materialBatch = ConcreteQuadBatch(MAX_TRIANGLES_IN_BATCH * 3, MAX_TRIANGLES_IN_BATCH, null)
    private val coloredBatch = ShapeRenderer(MAX_TRIANGLES_IN_BATCH * 3)
    private val texturedBatch = PolygonSpriteBatch(MAX_TRIANGLES_IN_BATCH * 3, MAX_TRIANGLES_IN_BATCH, null)
    private var state = RenderState.NOTHING

    private var lastMaterial: Material? = null

    private var lastAlbedo: Texture? = null
    private var lastNormals: Texture? = null
    private var lastColor: Color = Color.WHITE
    private var lastColorAsFloat: Float = Color.WHITE.toFloatBits()
    private var scissorArea: Box2i? = null

    private val WHITE = Color4f(1f, 1f, 1f, 1f)

    private var modelTransform = Matrix3f.IDENTITY

    override var environment = Environment(Matrix3f.IDENTITY, Matrix3f.IDENTITY, Colors.PURPLE, Vector3f.ONE)
        set(value) {
            field = value
            setupBlending()
            field.apply {
                updateMatrices()
            }
        }

    override fun flush() {
        materialBatch.flush()
        coloredBatch.flush()
        texturedBatch.flush()

        lastMaterial = null
        modelTransform = Matrix3f.IDENTITY
        setScissor(null)
    }

    override var viewPort: Dimension2i = Dimension2i.ZERO
        set(view) {
            glViewport(0, 0, view.width, view.height)
            field = view
        }

    override fun clear() {
        environment.backgroundColor.toColor4f().apply {
            Gdx.gl.glClearColor(red, green, blue, alpha)
        }
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT)
    }

    private fun updateMatrices() {
        val modelMatrix = modelTransform
        val transformMatrix = modelMatrix * environment.viewMatrix
        val projectionMatrix = environment.projectionMatrix

        coloredBatch.projectionMatrix = projectionMatrix.customMatrix4()
        coloredBatch.transformMatrix = transformMatrix.customMatrix4()

        texturedBatch.projectionMatrix = projectionMatrix.customMatrix4()
        texturedBatch.transformMatrix = transformMatrix.customMatrix4()

        materialBatch.setProjectionMatrix(projectionMatrix.customMatrix4())
        materialBatch.setTransformMatrix(transformMatrix.customMatrix4())

        materialBatch.lightPosition = modelMatrix.inverse().transform(environment.lightPosition).toVector3()
    }

    private fun Matrix3f.customMatrix4(): Matrix4 {
        return customMatrix4f().toMatrix4()
    }

    private fun Matrix3f.customMatrix4f(): Matrix4f {
        return Matrix4f(
            floatArrayOf(
                data[0], data[1], 0f, data[2],
                data[3], data[4], 0f, data[5],
                0f, 0f, 1f, 0f,
                data[6], data[7], 0f, data[8],
            )
        )
    }

    fun setup(material: Material, modelTransform: Matrix3f = Matrix3f.IDENTITY) {
        setModelTransform(modelTransform)

        if (lastMaterial == material) return
        setup(getState(material))

        lastMaterial = material
        lastColor = (material.color ?: WHITE).toColor()
        lastColorAsFloat = lastColor.toFloatBits()

        val albedo = material.albedoMap
        lastAlbedo = if (albedo != null) textureProvider.get(albedo) else null
        val normals = material.normalMap
        lastNormals = if (normals != null) textureProvider.get(normals) else null

        coloredBatch.color = lastColor
        setupBlending()
    }

    private fun setup(next: RenderState) {
        if (state == next) return

        if (state == RenderState.COLORED) coloredBatch.end()
        if (state == RenderState.TEXTURED) texturedBatch.end()
        if (state == RenderState.MATERIAL) materialBatch.end()

        if (next == RenderState.COLORED) coloredBatch.begin(ShapeRenderer.ShapeType.Filled)
        if (next == RenderState.TEXTURED) texturedBatch.begin()
        if (next == RenderState.MATERIAL) materialBatch.begin()

        state = next
    }

    private fun setupBlending() {
        val hasAlpha = true//lastColor.a < 1f || lastAlbedo?.textureData?.format == Pixmap.Format.RGBA8888
        if (hasAlpha) {
            Gdx.gl.glEnable(GL20.GL_BLEND)
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
        } else {
            Gdx.gl.glDisable(GL20.GL_BLEND)
        }
    }

    private fun getState(material: Material): RenderState {
        if (material.normalMap != null && material.albedoMap != null) {
            return RenderState.MATERIAL
        }
        if (material.albedoMap != null) {
            return RenderState.TEXTURED
        }
        return RenderState.COLORED
    }

    override fun drawQuad(material: Material, quad: Quad2f) {
        setup(material)

        val state = state
        when (state) {
            RenderState.COLORED -> {
                coloredBatch.triangle(quad.getFace(0))
                coloredBatch.triangle(quad.getFace(1))
            }


            RenderState.TEXTURED -> {
                val texture = lastAlbedo!!
                val c = lastColorAsFloat
                val vertices = quad.run {
                    floatArrayOf(
                        v0.x, v0.y, c, 0f, 1f,
                        v1.x, v1.y, c, 1f, 1f,
                        v2.x, v2.y, c, 1f, 0f,
                        v3.x, v3.y, c, 0f, 0f,
                    )
                }
                texturedBatch.draw(texture, vertices, 0, vertices.size)
            }

            RenderState.MATERIAL -> {
                val albedo = lastAlbedo!!
                val normals = lastNormals!!
                val c = lastColorAsFloat
                val tangent = (quad.v1 - quad.v0).normalize()

                val pad = Geometry2Float.getPointAroundRay(quad.v0, quad.v1, quad.v2, 0f)
                val yScale = if (pad == PointAroundRay.RIGHT) -1f else 1f

                val vertices = quad.run {
                    floatArrayOf(
                        v0.x, v0.y, c, 0f, 0f, tangent.x, tangent.y,
                        v1.x, v1.y, c, 1f, 0f, tangent.x, tangent.y,
                        v2.x, v2.y, c, 1f, yScale, tangent.x, tangent.y,
                        v3.x, v3.y, c, 0f, yScale, tangent.x, tangent.y,
                    )
                }
                materialBatch.drawQuad(albedo, normals, vertices, 0, vertices.size)
            }

            RenderState.NOTHING -> {

            }
        }
    }

    override fun drawTriangle(material: Material, triangle: Triangle2f) {
        setup(material)

        val state = state
        when (state) {
            RenderState.COLORED -> {
                coloredBatch.triangle(triangle)
            }


            RenderState.TEXTURED -> {
                val texture = lastAlbedo!!
                val c = lastColorAsFloat
                val floatPerRecord = 5
                val vertices = triangle.run {
                    floatArrayOf(
                        v0.x, v0.y, c, 1f, 0f,
                        v1.x, v1.y, c, 0f, 0f,
                        v2.x, v2.y, c, 0f, 1f,
                    )
                }
                texturedBatch.draw(texture, vertices, 0, 3 * floatPerRecord, shortArrayOf(0, 1, 2), 0, 3)
            }

            RenderState.MATERIAL -> {
                val albedo = lastAlbedo!!
                val normals = lastNormals!!
                val c = lastColorAsFloat
                val tangent = (triangle.v1 - triangle.v0).normalize()

                val pad = Geometry2Float.getPointAroundRay(triangle.v0, triangle.v1, triangle.v2, 0f)
                val yScale = if (pad == PointAroundRay.RIGHT) -1f else 1f

                val vertices = triangle.run {
                    floatArrayOf(
                        v0.x, v0.y, c, 0f, 0f, tangent.x, tangent.y,
                        v1.x, v1.y, c, 1f, 0f, tangent.x, tangent.y,
                        v2.x, v2.y, c, 0f, yScale, tangent.x, tangent.y,
                    )
                }
                materialBatch.drawTriangle(albedo, normals, vertices, 0, vertices.size)
            }

            RenderState.NOTHING -> {

            }
        }
    }

    override fun getScissor(): Box2i? {
        return scissorArea
    }

    override fun setScissor(area: Box2i?) {
        if (this.scissorArea == area) return

        if (area == null) {
            scissorArea = null
            Gdx.gl.glDisable(GL_SCISSOR_TEST)
        } else {
            val clamped = area//Box2i(area.min.clamp(Vector2i.ZERO, viewportMax), area.max.lower(viewportMax))

            Gdx.gl.glScissor(clamped.min.x, viewPort.height - clamped.max.y - 1, clamped.dimension.x, clamped.dimension.y)
            Gdx.gl.glEnable(GL_SCISSOR_TEST)
            scissorArea = clamped
        }
    }

    override fun drawGraphic(graphic: VectorGraphic, modelTransform: Matrix3f) {
        setup(graphic.material, modelTransform)

        val state = state
        when (state) {
            RenderState.COLORED -> {
                drawColoredGraphic(graphic.geometry)
            }

            RenderState.TEXTURED -> {
                drawTexturedGraphic(graphic.geometry)
            }

            RenderState.MATERIAL -> {
                drawTexturedWithNormalGraphic(graphic.geometry)
            }

            RenderState.NOTHING -> {

            }
        }
    }

    private fun drawColoredGraphic(geometry: Geometry) {
        val vertices = geometry.vertices
        val indices = geometry.indices
        var remainsVertices = (coloredBatch.renderer.maxVertices - coloredBatch.renderer.numVertices) / 3 * 3
        indices.forEach { vertexId ->
            if (remainsVertices == 0) {
                coloredBatch.flush()
                remainsVertices = coloredBatch.renderer.maxVertices / 3 * 3
            }

            vertices.getColor(vertexId.toInt()).apply {
                coloredBatch.renderer.color(red, green, blue, alpha)
            }
            vertices.getPosition2(vertexId.toInt()).apply {
                coloredBatch.renderer.vertex(x, y, 0f)
            }
            remainsVertices--
        }
    }

    private fun drawTexturedGraphic(geometry: Geometry) {
        val vertices = geometry.vertices
        val indices = geometry.indices
        val albedo = lastAlbedo!!
        val triangleAmount = indices.size / 3
        val color = lastColorAsFloat
        val vertexSize = 5
        var localOffset = 0
        val triangleVertices = FloatArray(3 * vertexSize * min(MAX_TRIANGLES_IN_BATCH, triangleAmount))
        repeat(triangleAmount) { triangleId ->
            repeat(3) { localVetexId ->
                val vertexId = indices[triangleId * 3 + localVetexId]

                vertices.getPosition2(vertexId.toInt()).apply {
                    triangleVertices[localOffset++] = x
                    triangleVertices[localOffset++] = y
                }
                if (vertices.attributes.hasColor) {
                    vertices.getColor(vertexId.toInt()).apply {
                        triangleVertices[localOffset++] = toFloatColor().value
                    }
                } else {
                    //materialBatch need color value anyway
                    triangleVertices[localOffset++] = color
                }
                vertices.getTextureCoord(vertexId.toInt()).apply {
                    triangleVertices[localOffset++] = x
                    triangleVertices[localOffset++] = y
                }
            }
            if (localOffset == triangleVertices.size) {
                val verticesCount = localOffset / vertexSize
                texturedBatch.draw(albedo, triangleVertices, 0, localOffset, IndicesProvider.indices, 0, verticesCount)
                localOffset = 0
            }
        }
        val verticesCount = localOffset / vertexSize
        texturedBatch.draw(albedo, triangleVertices, 0, localOffset, IndicesProvider.indices, 0, verticesCount)
    }

    private fun drawTexturedWithNormalGraphic(geometry: Geometry) {
        val vertices = geometry.vertices
        val indices = geometry.indices
        val albedo = lastAlbedo!!
        val normals = lastNormals!!
        val triangleAmount = indices.size / 3
        val vertexSize = materialBatch.VERTEX_SIZE
        val color = lastColorAsFloat
        var localOffset = 0

        val triangleVertices = FloatArray(3 * vertexSize * min(MAX_TRIANGLES_IN_BATCH, triangleAmount))
        repeat(triangleAmount) { triangleId ->

            repeat(3) { localVetexId ->
                val vertexId = indices[triangleId * 3 + localVetexId]

                vertices.getPosition2(vertexId.toInt()).apply {
                    triangleVertices[localOffset++] = x
                    triangleVertices[localOffset++] = y
                }
                if (vertices.attributes.hasColor) {
                    vertices.getColor(vertexId.toInt()).apply {
                        triangleVertices[localOffset++] = toFloatColor().value
                    }
                } else {
                    //materialBatch need color value anyway
                    triangleVertices[localOffset++] = color
                }
                vertices.getTextureCoord(vertexId.toInt()).apply {
                    triangleVertices[localOffset++] = x
                    triangleVertices[localOffset++] = y
                }
                vertices.getTangent(vertexId.toInt()).apply {
                    triangleVertices[localOffset++] = x
                    triangleVertices[localOffset++] = y
                }
            }
            if (localOffset == triangleVertices.size) {
                materialBatch.drawTriangle(albedo, normals, triangleVertices, 0, localOffset)
                localOffset = 0
            }
        }
        materialBatch.drawTriangle(albedo, normals, triangleVertices, 0, localOffset)
    }

    private fun setModelTransform(value: Matrix3f) {
        if (modelTransform == value) return
        modelTransform = value
        updateMatrices()
    }


    override fun dispose() {
        coloredBatch.dispose()
        texturedBatch.dispose()
    }
}

