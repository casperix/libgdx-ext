package casperix.demo.custom

import casperix.demo.impl.AbstractDemo
import casperix.demo.impl.shape_builder.BezierCubicEditor
import casperix.demo.util.CurveHelperExt
import casperix.math.curve.CurveHelper
import casperix.math.curve.float32.UniformCurve2f
import casperix.math.random.nextBezierCubic2f
import casperix.math.vector.float32.Vector2f
import casperix.misc.toPrecision
import casperix.renderer.Renderer2D
import kotlin.random.Random


class UniformBezierCubicDemo(val random: Random) : AbstractDemo() {
    private val bezierBuilder = BezierCubicEditor(pointManager, random.nextBezierCubic2f())
    private var uniform = UniformCurve2f(bezierBuilder.shape)

    init {
        pointManager.groups += bezierBuilder.getPoints()
    }

    override fun getDetail(): String {
        val bezier = bezierBuilder.shape
        val info = mutableListOf<String>()

        listOf(2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192).forEach { parts ->
            val test = UniformCurve2f(bezier, parts)
            val rough1 = CurveHelperExt.getRough(test, 1000)
            val rough2 = CurveHelper.getRough(test, 1000)

            info += "$parts => ${rough1.toPrecision(4)} => ${rough2.toPrecision(4)}"
        }


        return info.joinToString("\n")
    }

    override fun update(customPoint: Vector2f) {
        val bezier = bezierBuilder.shape
        uniform = UniformCurve2f(bezier, 100)
    }

    override fun renderScene(renderer: Renderer2D) {
        renderer.drawCurve(uniform)
        renderer.drawCurvePoints(uniform)
        renderer.drawCurveNormals(uniform)
    }


}

