package casperix.demo.impl

import casperix.input.InputEvent
import casperix.math.vector.float32.Vector2f
import casperix.renderer.Renderer2D

interface Demo {
    fun getDetail(): String
    fun update(customPoint: Vector2f)

    fun render(renderer: Renderer2D)
    fun input(event: InputEvent)
}