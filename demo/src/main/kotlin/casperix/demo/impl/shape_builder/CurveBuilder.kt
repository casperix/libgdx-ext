package casperix.demo.impl.shape_builder

import casperix.demo.impl.control_point.ControlPoint
import casperix.demo.impl.control_point.ControlPointManager
import casperix.math.curve.float32.Curve2f

interface CurveBuilder {
    fun getPoints(): List<ControlPoint>
    fun build(): Curve2f

    fun register(pointManager: ControlPointManager) {
        pointManager.groups += getPoints()
    }
}